package com.tipersonal.dao;

import com.tipersonal.model.Flow;
import com.tipersonal.model.Product;
import com.tipersonal.model.Status;
import com.tipersonal.model.TestCase;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TestCaseDAO extends ConnectionUtil {

    public List<TestCase> getTestCases() {
        Connection conn = conDB();
        StringBuilder sql = new StringBuilder();
        PreparedStatement ps;
        ResultSet rs;
        TestCase test;
        List<TestCase> list = new ArrayList<>();
        try {
            sql.append("SELECT * FROM CasoDeTeste");
            ps = conn.prepareStatement(sql.toString());
            rs = ps.executeQuery();
            while (rs.next()) {
                test = new TestCase();
                test.setId(rs.getInt("id"));
                test.setName(rs.getString("nome"));
                test.setDescription(rs.getString("descricao"));
                test.setPrecondition(rs.getString("precondicao"));
                test.setAssigned(rs.getString("atribuido"));
                test.setOrder(rs.getInt("ordem"));
                test.setStatus(new Status(rs.getInt("idStatus")));
                test.setProduct(new Product(rs.getInt("idProduto")));
                test.setFlow(new Flow(rs.getInt("idFluxo")));
                test.setCreated(rs.getDate("dataCriacao"));
                test.setUpdated(rs.getDate("dataAlteracao"));
                list.add(test);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("(getTestCases) - Error: " + e.getMessage());

        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    public void insert(TestCase test) throws SQLException {
        Connection conn = conDB();
        StringBuilder sql = new StringBuilder();
        PreparedStatement ps;
        int index = getTestCases().size();
        try {
            sql.delete(0, sql.length());
            sql.append("INSERT INTO CasoDeTeste (id, nome, descricao, precondicao, atribuido, ordem, idStatus, idProduto, idFluxo, dataCriacao, dataAlteracao)");
            sql.append("VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            ps = conn.prepareStatement(sql.toString());
            ps.setInt(1, ++index);
            ps.setString(2, test.getName());
            ps.setString(3, test.getDescription());
            ps.setString(4, test.getPrecondition());
            ps.setString(5, test.getAssigned());
            ps.setInt(6, test.getOrder());
            ps.setInt(7, test.getStatus().getId());
            ps.setInt(8, test.getProduct().getId());
            ps.setInt(9, test.getFlow().getId());
            ps.setDate(10, test.getCreated());
            ps.setDate(11, test.getUpdated());
            ps.executeUpdate();
        } finally {
            conn.close();
        }
    }

    public void update(List<TestCase> list, TestCase test) {
        for (int liX = list.size() - 1; liX >= 0; liX --) {
            if (list.get(liX).getId() == test.getId()) {
                list.get(liX).setName(test.getName());
                list.get(liX).setDescription(test.getDescription());
                list.get(liX).setPrecondition(test.getPrecondition());
                list.get(liX).setAssigned(test.getAssigned());
                list.get(liX).setOrder(test.getOrder());
                list.get(liX).setStatus(test.getStatus());
                list.get(liX).setProduct(test.getProduct());
                list.get(liX).setFlow(test.getFlow());
                list.get(liX).setCreated(test.getCreated());
                list.get(liX).setUpdated(test.getUpdated());
            }
        }
    }

    public void delete(List<TestCase> list, TestCase test) {
        for (int liX = list.size() - 1; liX >= 0; liX --) {
            if (list.get(liX).getId() == test.getId())
                list.remove(liX);
        }
    }
}

package com.tipersonal.dao;

import com.tipersonal.model.Flow;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class FlowDAO extends ConnectionUtil{

    private List<Flow> list;

    public FlowDAO() {
        this.list = getFlowList();
    }

    private List<Flow> getFlowList() {
        Connection conn = conDB();
        StringBuilder sql = new StringBuilder();
        PreparedStatement ps;
        ResultSet rs;
        Flow flow;
        list = new ArrayList<>();
        try {
            sql.append("SELECT * FROM Fluxo");
            ps = conn.prepareStatement(sql.toString());
            rs = ps.executeQuery();
            while (rs.next()) {
                flow = new Flow();
                flow.setId(rs.getInt("id"));
                flow.setName(rs.getString("nome"));
                flow.setDescription(rs.getString("descricao"));
                list.add(flow);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("(getFlowList) - Error: " + e.getMessage());
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    public List<String> getFlowListName() {
        List<String> listName = new ArrayList<>();
        list.forEach(e -> listName.add(e.getName()));
        return listName;
    }

    public Flow getFlowByName(String name){
        return list.stream()
                .filter(e -> e.getName().equals(name))
                .findAny().orElse(null);
    }
}

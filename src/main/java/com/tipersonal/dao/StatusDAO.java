package com.tipersonal.dao;

import com.tipersonal.model.Status;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class StatusDAO extends ConnectionUtil{

    private List<Status> list;

    public StatusDAO() {
        this.list = getStatusList();
    }

    private List<Status> getStatusList() {
        Connection conn = conDB();
        StringBuilder sql = new StringBuilder();
        PreparedStatement ps;
        ResultSet rs;
        Status status;
        list = new ArrayList<>();
        try {
            sql.append("SELECT * FROM Status");
            ps = conn.prepareStatement(sql.toString());
            rs = ps.executeQuery();
            while (rs.next()) {
                status = new Status();
                status.setId(rs.getInt("id"));
                status.setName(rs.getString("nome"));
                status.setColor(rs.getString("cor"));
                list.add(status);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("(getStatusList) - Error: " + e.getMessage());
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return this.list;
    }

    public List<String> getStatusListName() {
        List<String> listName = new ArrayList<>();
        list.forEach(e -> listName.add(e.getName()));
        return listName;
    }

    public Status getStatusByName(String name){
        return list.stream()
                .filter(e -> e.getName().equals(name))
                .findAny().orElse(null);
    }
}

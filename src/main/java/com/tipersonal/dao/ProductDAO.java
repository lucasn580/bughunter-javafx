package com.tipersonal.dao;

import com.tipersonal.model.Product;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProductDAO extends ConnectionUtil {

    private List<Product> list;

    public ProductDAO() {
        this.list = getProductList();
    }

    private List<Product> getProductList() {
        Connection conn = conDB();
        StringBuilder sql = new StringBuilder();
        PreparedStatement ps;
        ResultSet rs;
        Product product;
        list = new ArrayList<>();
        try {
            sql.append("SELECT * FROM Produto");
            ps = conn.prepareStatement(sql.toString());
            rs = ps.executeQuery();
            while (rs.next()) {
                product = new Product();
                product.setId(rs.getInt("id"));
                product.setName(rs.getString("nome"));
                product.setDescription(rs.getString("descricao"));
                list.add(product);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("(getProductList) - Error: " + e.getMessage());
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    public List<String> getProductListName() {
        List<String> listName = new ArrayList<>();
        list.forEach(e -> listName.add(e.getName()));
        return listName;
    }

    public Product getProductByName(String name){
        return list.stream()
                .filter(e -> e.getName().equals(name))
                .findAny().orElse(null);
    }
}

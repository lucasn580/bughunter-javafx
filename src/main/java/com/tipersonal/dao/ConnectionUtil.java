package com.tipersonal.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author lucasns
 * @since #1.0
 */
public class ConnectionUtil {

    public static Connection conDB() {
        try {
            Connection con = DriverManager.getConnection("jdbc:mysql://tipersonal.com:3306/bughunter", "lucasns", "lEjwc3DWKTR7");
            return con;
        } catch (SQLException ex) {
            System.err.println("ConnectionUtil: "+ex.getMessage());
           return null;
        }
    }
}

package com.tipersonal.model;

public class Flow {
    private int id;
    private String name;
    private String description;

    public Flow() {
    }

    public Flow(int idFluxo) {
        this.id = idFluxo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

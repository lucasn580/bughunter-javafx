package com.tipersonal.model;

public class Status {
    private int id;
    private String name;
    private String color;

    public Status() {
    }

    public Status(int idStatus) {
        this.id = idStatus;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}

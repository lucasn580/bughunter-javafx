package com.tipersonal.controllers;

import com.tipersonal.dao.FlowDAO;
import com.tipersonal.dao.ProductDAO;
import com.tipersonal.dao.StatusDAO;
import com.tipersonal.dao.TestCaseDAO;
import com.tipersonal.model.TestCase;
import com.tipersonal.util.DateUtils;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

/**
 * @author lucasns
 * @since #1.0
 */
public class TestCaseController implements Initializable {

    @FXML
    private ComboBox<String> cbxStatus;
    @FXML
    private Spinner<Integer> spnOrder;
    @FXML
    private ComboBox<String> cbxProduct;
    @FXML
    private ComboBox<String> cbxFlow;
    @FXML
    private TextField txtTestName;
    @FXML
    private TextArea txtPrecondition;
    @FXML
    private TextArea txtDescription;
    @FXML
    private TextField txtAssigned;
    @FXML
    private Button btnSave;
    @FXML
    Label lblStatus;

    @FXML
    TableView<TestCase> tblData;

    private ObservableList<TestCase> data;
    private StatusDAO statusDAO;
    private ProductDAO productDAO;
    private FlowDAO flowDAO;
    /**
     * Initializes the controller class.
     */
    public TestCaseController() {
        data = FXCollections.observableArrayList(
                new TestCaseDAO().getTestCases()
        );
        statusDAO = new StatusDAO();
        productDAO = new ProductDAO();
        flowDAO = new FlowDAO();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cbxStatus.getItems().addAll(statusDAO.getStatusListName());
        cbxStatus.getSelectionModel().select(0);
        cbxProduct.getItems().addAll(productDAO.getProductListName());
        cbxProduct.getSelectionModel().select(0);
        cbxFlow.getItems().addAll(flowDAO.getFlowListName());
        cbxFlow.getSelectionModel().select(0);
        spnOrder.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 999));
        fillTable();
    }

    @FXML
    public void HandleEvents(MouseEvent event) {
        String mgs = validateFields();
        if (mgs != null) {
            lblStatus.setTextFill(Color.TOMATO);
            lblStatus.setText(mgs);
        } else {
            doPost();
        }
    }

    private void clearFields() {
        txtTestName.clear();
        txtDescription.clear();
        txtPrecondition.clear();
        txtAssigned.clear();
    }

    private String doPost() {
        try {
            TestCase testCase = new TestCase();
            testCase.setStatus(statusDAO.getStatusByName(cbxStatus.getValue()));
            testCase.setProduct(productDAO.getProductByName(cbxProduct.getValue()));
            testCase.setFlow(flowDAO.getFlowByName(cbxFlow.getValue()));
            testCase.setOrder(spnOrder.getValue());
            testCase.setName(txtTestName.getText());
            testCase.setDescription(txtDescription.getText());
            testCase.setPrecondition(txtPrecondition.getText());
            testCase.setAssigned(txtAssigned.getText());
            testCase.setCreated(DateUtils.getCurrentDate());
            testCase.setUpdated(DateUtils.getCurrentDate());
            TestCaseDAO testCaseDAO = new TestCaseDAO();
            testCaseDAO.insert(testCase);
            lblStatus.setTextFill(Color.GREEN);
            lblStatus.setText("Added Successfully");

            fillTable();
            //clear fields
            clearFields();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            lblStatus.setTextFill(Color.TOMATO);
            lblStatus.setText(ex.getMessage());
            return "Exception";
        }
        return "Success";
    }

    private String validateFields() {
        if (cbxStatus.getValue() == null)
            return "Select status";
        else if (cbxProduct.getValue() == null)
            return "Select product";
        else if (cbxFlow.getValue() == null)
            return "Select flow";
        else if (cbxStatus.getValue() == null)
            return "Select status";
        else if (txtTestName.getText().isEmpty())
            return "Enter the name";
        else
            return null;
    }

    //only fetch columns
    private void fillTable() {
        FilteredList<TestCase> filteredData = new FilteredList<>(data, p -> true);
        //Wrap the FilteredList in a SortedList.
        SortedList<TestCase> sortedData = new SortedList<>(filteredData);
        //Bind the SortedList comparator to the TableView comparator.
        sortedData.comparatorProperty().bind(tblData.comparatorProperty());
        //Add sorted (and filtered) data to the table.
        tblData.setItems(sortedData);
    }
}

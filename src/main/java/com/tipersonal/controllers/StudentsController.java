package com.tipersonal.controllers;

import com.tipersonal.dao.TestCaseDAO;
import com.tipersonal.model.TestCase;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

/**
 * @author lucasns
 * @since #1.0
 */
public class StudentsController implements Initializable {

    @FXML
    private TableView<TestCase> tbData;
    @FXML
    public TableColumn<TestCase, Integer> testCaseId;
    @FXML
    public TableColumn<TestCase, String> name;
    @FXML
    public TableColumn<TestCase, String> created;
    @FXML
    public TextField txtSearch;

    public StudentsController() throws SQLException {

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        testCaseId.setCellValueFactory(new PropertyValueFactory<>("Id"));
        name.setCellValueFactory(new PropertyValueFactory<>("Name"));
        created.setCellValueFactory(new PropertyValueFactory<>("Created"));

        FilteredList<TestCase> filteredData = new FilteredList<>(testCasesModels, p -> true);
        txtSearch.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(myObject -> {
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                String lowerCaseFilter = newValue.toLowerCase();

                if (String.valueOf(myObject.getName()).toLowerCase().contains(lowerCaseFilter)) {
                    return true;

                } else if (String.valueOf(myObject.getCreated()).toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                }
                return false;
            });
        });
        //Wrap the FilteredList in a SortedList.
        SortedList<TestCase> sortedData = new SortedList<>(filteredData);
        //Bind the SortedList comparator to the TableView comparator.
        sortedData.comparatorProperty().bind(tbData.comparatorProperty());
        //Add sorted (and filtered) data to the table.
        tbData.setItems(sortedData);
    }

    private ObservableList<TestCase> testCasesModels = FXCollections.observableArrayList(
            new TestCaseDAO().getTestCases()
    );
}

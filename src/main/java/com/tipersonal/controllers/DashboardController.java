package com.tipersonal.controllers;

import com.tipersonal.dao.TestCaseDAO;
import com.tipersonal.model.TestCase;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.PieChart;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

/**
 * @author lucasns
 * @since #1.0
 */
public class DashboardController implements Initializable {

    @FXML
    private TableView<TestCase> tbData;
    @FXML
    public TableColumn<TestCase, Integer> testCaseId;
    @FXML
    public TableColumn<TestCase, String> name;
    @FXML
    public TableColumn<TestCase, String> created;
    @FXML
    private PieChart pieChart;

    public DashboardController() throws SQLException {
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        loadChart();
        loadStudents();
    }

    private void loadChart() {
        PieChart.Data slice1 = new PieChart.Data("Classes", 213);
        PieChart.Data slice2 = new PieChart.Data("Attendance"  , 67);
        PieChart.Data slice3 = new PieChart.Data("Teachers" , 36);

        pieChart.getData().add(slice1);
        pieChart.getData().add(slice2);
        pieChart.getData().add(slice3);
    }

    private ObservableList<TestCase> studentsModels = FXCollections.observableArrayList(
            new TestCaseDAO().getTestCases()
    );

    private void loadStudents() {
        testCaseId.setCellValueFactory(new PropertyValueFactory<>("Id"));
        name.setCellValueFactory(new PropertyValueFactory<>("Name"));
        created.setCellValueFactory(new PropertyValueFactory<>("Created"));
        tbData.setItems(studentsModels);
    }
}
